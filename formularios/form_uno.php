<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Formulario</title>
    <link rel="stylesheet" href="bootstrap.min.css">
    <script src="jquery-3.7.0.min.js"></script>
    <script src="jquery.validate.min.js"></script>
    <script src="bootstrap.min.js"></script>
</head>
<body>
    <h1>Formulario lado PHP</h1>
    <hr>
    <div class="container">
        <div class="col-md-12 well">
            Nuevo contacto nombre ($_POST): <?php echo $_POST['nombre']; ?>
        </div>

        <div class="col-md-12 well">
            Nuevo contacto nombre ($_REQUEST): <?php echo $_REQUEST['nombre']; ?>
        </div>
        <hr>
        <div class="col-md-12 well">
            Nuevo contacto email ($_POST): <?php echo $_POST['email']; ?>
        </div>

        <div class="col-md-12 well">
            Nuevo contacto email ($_REQUEST): <?php echo $_REQUEST['email']; ?>
        </div>
    </div>
    <hr>
    <p class="h6">
        Variables de tipo <i>array</i> usadas para la recogida de la información son: <mark>$_POST</mark> y <mark>$_REQUEST</mark>. Las dos almacenan la información del formulario enviado como un <i>array</i> por lo que es necesario indicar un índice, en este caso asociativo, para acceder a sus valores.
    </p>
    <p class="h6">
        <b>$_POST</b> contiene toda la información enviada por un formulario a una URL. <br>
        <b>$_GET</b> contiene toda la información enviada por un formulario o una URL. <br>
        <b>$_REQUEST</b> contiene toda la información enviada por un formulario o una URL ya sea utilizando <i>POST</i> o <i>GET</i>. También incluye la información relacionada con las cookies, disponibles en la variable <b>$_COOKIE</b>.
    </p>
</body>
</html>