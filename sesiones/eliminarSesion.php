<?php 
session_start();
$_SESSION = array();            //asegura borrar datos de la sesión
session_regenerate_id(TRUE);    //fuerza regenerar cookie
session_destroy();
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Session</title>
    <link rel="stylesheet" href="bootstrap.min.css">
    <script src="bootstrap.min.js"></script>
    <script src="jquery-3.7.0.min.js"></script>
    <script src="jquery.validate.min.js"></script>
</head>
<body>
    <div class="container">
        <h1>Eliminando una sesión</h1>
        <hr>
        <p>
            Si actualiza la página, la sesión seguirá vigente, por lo que aparecerá el mismo identificador siempre, hasta que expire el tiempo de sesión o haga clic en el enlace a la página que va a crear a continuación en la que destruiremos la sesión.
        </p>
        <hr>
        <div class="col-md-12 well">
            <?php echo "Ya no existe la sesión"; ?>
        </div>
        <div class="col-md-12 well">
            <div class="span12">
                <a href="crearSesion.php">Crear sesión</a>
            </div>
            <div class="span12">
                <a href="crearSesionConDatos.php">Crear Sesión con Datos</a>
            </div>
        </div>
        <hr>
        <p>
            Para destruir una sesión y asegurarnos de que se han eliminado todos sus datos es necesario realizar varias acciones: recuperar la sesión, reiniciar la variable superglobal, regenerar la cookie de sesión y finalmente destruir la sesión. <br>
            <ul>
                <li>session_start()</li>
                <li>$_SESSION</li>
                <li>session_regenerate_id()</li>
                <li>session_destroy()</li>
            </ul>
        </p>
    </div>
</body>
</html>