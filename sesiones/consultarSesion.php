<?php session_start(); ?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Consulta-Sesion</title>
    <link rel="stylesheet" href="bootstrap.min.css">
    <script src="bootstrap.min.js"></script>
    <script src="jquery-3.7.0.min.js"></script>
    <script src="jquery.validate.min.js"></script>
</head>
<body>
    <h1>Consultar Datos con Sesión</h1>
    <hr>
    <div class="container">
        <h2>Consultando los datos de la sesión</h2>
        <div class="col-md-12 well">
            Nombre: <?php echo $_SESSION["nombre"]; ?>
        </div>
        <div class="col-md-12 well">
            Profesión: <?php echo $_SESSION["profesion"]; ?>
        </div>
        <div class="col-md-12 well">
            <div class="span12">
                <a href="eliminarSesion.php">Eliminar Sesión</a>
            </div>
        </div>
    </div>
</body>
</html>