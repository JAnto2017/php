<?php session_start(); ?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Sesiones</title>
    <link rel="stylesheet" href="bootstrap.min.css">
    <script src="bootstrap.min.js"></script>
    <script src="jquery-3.7.0.min.js"></script>
    <script src="jquery.validate.min.js"></script>
</head>
<body>
    <div class="container">
        <h1>Creando una Sesión</h1>
        <div class="col-md-12 well">
            <?php echo "El identificador de la sesión es: ".session_id(); ?>
        </div>
        <div class="col-md-12 well">
            <div class="span12">
                <a href="eliminarSesion.php">Destruir Sesión</a>
            </div>
        </div>
        <hr>
        <p>
            Al principio de la página, antes de la etiqueta HTML, llamamos a la función <b>session_start()</b>. Desde este momento se crea la sesión y se le asigna el identificador que muestra la página invocando a la función <b>session_id()</b>
        </p>
    </div>
</body>
</html>