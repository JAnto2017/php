<?php session_start(); ?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Session</title>
    <link rel="stylesheet" href="bootstrap.min.css">
    <script src="bootstrap.min.js"></script>
    <script src="jquery-3.7.0.min.js"></script>
    <script src="jquery.validate.min.js"></script>
</head>
<body>
    <div class="container">
        <h1>Creando una sesión con datos</h1>
        <hr>
        <div class="col-md-12 well">
            <?php
                $_SESSION["nombre"] = "Jose Antonio";
                $_SESSION["profesion"] = "Astronauta";
            ?>
            Datos de sesión creados.
        </div>
        <div class="col-md-12 well">
            <a href="consultarSesion.php">Consultar datos de sesión</a>
        </div>
    </div>
</body>
</html>