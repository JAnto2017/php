<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Clases</title>
</head>
<body>
    <h1>Objetos / Clases</h1>
    <hr>
    <?php
    require_once("clase_uno.php");
    $jose = new Persona();
    $jose->setNombre("José Antonio");
    $jose->setApellidos("Delgado");
    $jose->setDni("0002224445A");
    ?>
    <hr>
    <h2>Los datos obtenidos de la clase</h2>
    <br>
    <h3>
        Datos: <?=
            $jose->getNombre() . " " . $jose->getApellidos();
        ?>
    </h3>
    
    <h3>
        DNI: <?= $jose->getDni(); ?>
    </h3>
    
</body>
</html>