<?php
/**
 * La clase tiene 3 propiedades y 6 métodos setter y getter.
 * Los métodos set -> dan valores a una propiedad.
 * Los métodos get -> obtienen valores.
 */
class Persona {
    public $nombre;
    public $apellidos;
    public $dni;

    function setNombre($nombre) {
        $this->nombre = $nombre;
    }

    function getNombre() {
        return $this->nombre;
    }

    function setApellidos($apellidos) {
        $this->apellidos = $apellidos;
    }

    function getApellidos() {
        return $this->apellidos;
    }

    function setDni($dni){
        $this->dni = $dni;
    }

    function getDni() {
        return $this->dni;
    }
}
?>