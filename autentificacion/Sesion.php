<?php

/**
 * Clase para gestionar sesiones de la aplicación.
 */

class Sesion {
    // ------------------------------------------------------------
    function __construct() {
        session_start();
    }
    // ------------------------------------------------------------
    public function getVariable($variable) {
        if (isset($_SESION[$variable])) {
            return $_SESION[$variable];
        }
        return null;
    }
    // ------------------------------------------------------------
    public function setVariable($variable, $valor){
        if (isset($_SESION[$variable])) {
            $_SESION[$variable] = $valor;
        }
    }
    // ------------------------------------------------------------
    public function terminarSesion() {
        session_start();
        $_SESION = array();
        session_regenerate_id(TRUE);
        session_destroy();
    }
    // ------------------------------------------------------------
}
?>