<?php 

/**
 * Clase para gestionar los usuarios que accederán a la aplicación
 * 
 */

require_once "Sesion.php"; 

class Usuario {
    private $usuarios;

    function __construct() {
        $this->usuarios = array (
            array(
                "username"  => "janto",
                "password"  => "1234",
                "nombre"    =>  "José",
                "apellido"  =>  "Delgado",
                "perfil"    =>  "Técnico",
                "teléfono"  =>  555123123
            ),
            array (
                "username"  => "ale123",
                "password"  => "1234567",
                "nombre"    =>  "Alejandro",
                "apellido"  =>  "Cabezas",
                "perfil"    =>  "Usuario",
                "teléfono"  =>  555656565
            ),
        );
    }
    // ------------------------------------------------------------
    public function existeUsuario($username) {
        foreach($this->usuarios as $usuario){
            if($usuario["username"] == $username) {
                return true;
            }
        }
        return false;
    }
    // ------------------------------------------------------------
    public function comprobarClave($username, $clave) {
        foreach($this->usuarios as $usuario){
            if ($usuario["username"] == $username && $usuario["password"] == $clave) {
                return true;
            }
        }
        return false;
    }
    // ------------------------------------------------------------
    private function getUsuario($username){
        foreach($this->usuarios as $usuario){
            if ($usuario["username"] == $username) {
                return $usuario;
            }
        }
        return array();
    }
    // ------------------------------------------------------------
    public function login() {
        if (isset($_POST["username"]) && isset($_POST["password"])) {
            $username = $_POST["username"];
            $password = $_POST["password"];

            if ($this->existeUsuario($username) == true && $this->comprobarClave($username, $password) == true) {
                $_SESSION = $this->getUsuario($username);
            } else {
                header("Location:login.html");
            }
        }
    }
    // ------------------------------------------------------------
    public function logout() {
        $session = new Sesion();
        $session->terminarSesion();
        header("Location:login.html");
    }
}
?>