<?php
/**
 * Fichero que recibe los datos del formulario de acceso.
 */
 require_once "Sesion.php";
 require_once "Usuario.php";
 $sesion = new Sesion();
 $usuario = new Usuario();
 $usuario->login();
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>ComprobarUsuario</title>
    <link rel="stylesheet" href="bootstrap.min.css">
    <script src="bootstrap.min.js"></script>
    <script src="jquery-3.7.0.min.js"></script>
    <script src="jquery.validate.min.js"></script>
</head>
<body>
    <div class="container">
        <h1>Acceso Permitido</h1>
        <div class="col-md-12 well">
            Nombre: <?php echo $_SESSION["nombre"]; ?>
        </div>
        <div class="col-md-12 well">
            Apellidos: <?php echo $_SESSION["apellido"]; ?>
        </div>
        <div class="col-md-12 well">
            Perfil: <?php echo $_SESSION["perfil"]; ?>
        </div>
        <div class="col-md-12 well">
            Teléfono: <?php echo $_SESSION["telefono"]; ?>
        </div>
        <div class="col-md-12 well">
            <div class="span12">
                <a href="logout.php">Salir</a>
            </div>
        </div>
    </div>
    
</body>
</html>
