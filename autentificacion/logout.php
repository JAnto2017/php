<?php
/**
 * Archivo para salir de la sesión.
 * Creamos una instancia de la clase USUARIO y llamamos a su método LOGOUT().
 * En este método pasamos la responsabilidad de destruir la sesión y sus datos a un objeto SESION a través del método TERMINARSESION().
 * Finalmente redirigimos al usuario de nuevo al formulario de acceso.
 */

 require_once "Sesion.php";
 require_once "Usuario.php";
 
 $usuario = new Usuario();

 $usuario->logout();

?>