<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Cookie</title>
    <link rel="stylesheet" href="bootstrap.min.css">
    <script src="bootstrap.min.js"></script>
    <script src="jquery-3.7.0.min.js"></script>
    <script src="jquery.validate.min.js"></script>
</head>
<body>
    <div class="container">
        <h1>Consultando Cookies</h1>
        <div class="col-md-12 well">
            <?php foreach($_COOKIE as $cookieNombre=>$cookieValor): ?>
            <div class="span12">Cookie '<?php echo $cookieNombre; ?>' con valor <?php echo $cookieValor; ?></div>
            <?php endforeach; ?>
        </div>
        <div class="col-md-12 well">
            <div class="span12"><a href="crearCookie.php">Crear cookies</a></div>
            <div class="span12"><a href="eliminarCookies.php">Eliminar cookies</a></div>
        </div>
        <hr>
        <div class="col-md-12 well">
            <p>
                En este archivo leemos y mostramos los valores de todas las <i>cookies</i> que haya creadas, utilizando la variable superglobal <b>$_COOKIE</b>, recorriendo sus valores utilizando la sentencia <b>foreach</b>. <br>
                Para comprobar que realmente se elimina la <i>cookie color_texto</i> espere un minuto y actualice la página de consulta de <i>cookies</i>.
            </p>
        </div>
    </div>
</body>
</html>