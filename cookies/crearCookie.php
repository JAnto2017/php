<?php setcookie("color_fondo", "red"); ?>
<?php setcookie("color_texto", "black", time()+60); ?>
<?php setcookie("columnas", 3); ?>
<?php setcookie("filas", 4); ?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Cookie</title>
    <link rel="stylesheet" href="bootstrap.min.css">
    <script src="jquery-3.7.0.min.js"></script>
    <script src="jquery.validate.min.js"></script>
    <script src="bootstrap.min.js"></script>
</head>
<body>
    
    <hr>
    <div class="container">
        <h1>Crear Cookie</h1>
        <div class="col-md-12 well">
            <div class="col-md-12">Creada Cookie 'color_fondo' con valor 'red'</div>
            <div class="col-md-12">Creada Cookie 'color_texto' con valor 'black' que expira en 60 seg.</div>
            <div class="col-md-12">Creada Cookie 'columnas' con valor 3</div>
            <div class="col-md-12">Creada Cookie 'filas' con valor 4</div>
        </div>
        <div class="col-md-12 well">
            <div class="col-md-12"><a href="consultarCookies.php">Consultar</a></div>
            <div class="col-md-12"><a href="eliminarCookies.php">Eliminar Cookies</a></div>
        </div>
        <div class="col-md-12 well">
            <p class="col-md-12">
                La creación de <i>cookies</i> en PHP debe hacerse siempre antes de dibujar cualquier elemento HTML ya que las cookies se envían con la cabecera de la página. No confundir con la sección <i>HEAD</i>.
            </p>
        </div>
    </div>
</body>
</html>