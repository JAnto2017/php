<?php setcookie("color_fondo",""); ?>
<?php setcookie("columnas",""); ?>
<?php setcookie("filas","") ?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Cookie</title>
    <link rel="stylesheet" href="bootstrap.min.css">
    <script src="bootstrap.min.js"></script>
    <script src="jquery-3.7.0.min.js"></script>
    <script src="jquery.validate.min.js"></script>
</head>
<body>
    <div class="container">
        <h1>Eliminando Cookies</h1>
        <div class="col-md-12 well">
            <div class="span12">Eliminada cookie 'color_fondo'</div>
            <div class="span12">Eliminada cookie 'color_texto'</div>
            <div class="span12">Eliminada cookie 'columnas'</div>
            <div class="span12">Eliminada cookie 'filas'</div>
        </div>
        <div class="col-md-12 well">
            <div class="span12"><a href="consultarCookies.php">Consultar cookies</a></div>
            <div class="span12"><a href="crearCookie.php">Crear cookies</a></div>
        </div>
    </div>
</body>
</html>