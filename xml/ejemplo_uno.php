<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>XML</title>
    <link rel="stylesheet" href="bootstrap.min.css">
    <script src="jquery-3.7.0.min.js"></script>
    <script src="jquery.validate.min.js"></script>
    <script src="bootstrap.min.js"></script>
</head>
<body>
    <h1>SimpleXML</h1>
    <hr>

    <?php
    $biblioteca = simplexml_load_file("biblioteca.xml");
    echo $biblioteca->tema->libro[0]->titulo;
    foreach ($biblioteca->tema as $tema) {
    ?>
        <h2><?= $tema["id"] ?></h2>
        <table class="table">
            <tr>
                <?php
                foreach ($tema->libro as $libro) {
                    ?>

                    <td>
                        <img src="<?= $libro->imagen ?>" align="left">
                        <b><?= $libro->titulo ?><br></b>
                        <?= $llibro->autor ?><br>
                        <?= $libro->editorial ?><br>
                    </td>
                    <?php
                }
    }
            ?>
            </tr>
        </table>       
        
      
</body>
</html>