<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>PDO - SQL</title>
</head>
<body>
    <h1>INSERT Filas - PDO</h1>
    <hr>
    <p>
        Inserción de datos. Se prepara la sentencia, se combinan los datos variables y se ejecutan. <br>
        Se hace uso de <b>bindValue()</b> para añadir varias variables a la sentencia. <br>
        Tras la ejecución, nos devuelve el último <i>id</i> insertado, haciendo uso de <b>lastInsertId()</b>.
    </p>
    <hr>

    <?php
    require_once("configuracion.php");

    $base_datos_PDO = new PDO("mysql:host=$servidor; dbname=$base_datos", $usuario, $pass);

    $sql = "INSERT INTO usuarios(nombre, apellidos, cuenta) VALUES (:nombre, :apellidos, :cuenta)";

    $sentencia = $base_datos_PDO->prepare($sql);
    $sentencia->bindValue(":nombre", 'Carlos');
    $sentencia->bindValue(":apellidos", 'Álvarez');
    $sentencia->bindValue(":cuenta", 3342);
    $sentencia->execute();

    echo "El último id es " . $base_datos_PDO->lastInsertId();
    ?>
</body>
</html>