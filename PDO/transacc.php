<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>PDO</title>
</head>
<body>
    <h1>Transacciones - PDO</h1>
    <hr>
    <p>
        La <i>Transacción</i> es una colección de consultas que deben ser ejecutadas en grupo. La peculariedad es que se está obligando a ejecutar el conjunto completo de sentencias o no ejecutar ninguna. <br>
        <i>Si la base de datos o la tabla no permite transacciones, puede obtener resultados inesperados en las consultas.</i> <br>
        Los pasos a ejecutar son: <br>
        <ul>
            <li>Iniciar transacción con <b>beginTransaction()</b>.</li>
            <li>Llamar al método <b>commit()</b>, tras la ejecución.</li>
            <li>Cancelar transacción si algo falla con el método <b>rooback()</b>.</li>
        </ul>
    </p>
    <hr>
    <?php
    require_once("configuracion.php");

    try {
        $base_datos_PDO = new PDO("mysql:host=$servidor; dbname=$base_datos", $usuario, $pass);
    } catch (PDOException $e){
        echo "Imposible conectar con la bse de datos";
        exit;
    }

    try {
        $base_datos_PDO->beginTransaction();
        $sql = "UPDATE usuarios SET cuenta=2 WHERE id=:id";

        $sentencia = $base_datos_PDO->prepare($sql);
        $sentencia->bindValue(":id", 1);
        $sentencia->execute();
        $sentencia->bindValue(":id", 2);
        $sentencia->execute();
        $sentencia->bindValue(":id", 3);
        $sentencia->execute();

        $base_datos_PDO->commit();

        echo "Transacción exitosa";

    } catch (PDOException $th) {
        $base_datos_PDO->rollBack();
        echo "Hubo algún error en la transacción" . $th->getMessage();
    }
    ?>
</body>
</html>