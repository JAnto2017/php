<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>PDO</title>
</head>
<body>
    <h1>PDO en PHP</h1>
    <hr>
    <p>
        La conexión se crea pasando un DSN <i>Data Source Name</i> que es un string que indica el tipo de base de datos, su dirección y otros detalles necesarios para su conexión. <br>
        Se captura la excepción <i>PDOException</i> si falla la conexión con la B.D. <br>
    </p>
    <hr>
    
    <?php

    try {
        require_once("configuracion.php");
        $base_datos_PDO = new PDO("mysql:host=$servidor; dbname=$base_datos; charset=utf8", $usuario, $pass);

    } catch (PDOException $ex) {
        echo ("Hubo algún error en la conexión PDO con B.D.");
        print($ex);
    }

    $resultado = $base_datos_PDO->query("SELECT * FROM usuarios");

    foreach ($resultado as $row) {
        echo $row['nombre'] . " " . $row['apellidos'] . '<br>';
    }
    ?>
</body>
</html>