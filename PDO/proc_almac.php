<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>PDO</title>
</head>
<body>
    <h1>Procedimientos Almacenados</h1>
    <hr>
    <p>
        Los <i>Procedimientos Almacenados</i> similares a las funciones, pero almacenados a nivel de base de datos. <br>
        Pueden tomar parámetros cuando son llamados para formar parte de sentencias preparadas. <br>
        Los procedimientos almacenados pueden ser llamados desde el código PHP o desde PDO. <br>
        Para ejecutar un procedimiento sólo tiene que hacer una llamada en el método <b>query()</b> con la palabra <b>call</b> seguido del nombre del procedimiento. Una vez hecha la consulta, se obtienen las filas y se muestran en pantalla.
    </p>
    <hr>

    <?php
    require_once("configuracion.php");
    $base_datos_PDO = new PDO("mysql:host=$servidor; dbname=$base_datos", $usuario, $pass);

    echo "Conexión con base de datos OK <br>";

    $resultado = $base_datos_PDO->query('call obtener_usuarios()');

    echo "query + call OK <br>";
    echo "<br>";
    
    $filas = $resultado->fetchAll();

    print_r($filas);

    ?>
</body>
</html>