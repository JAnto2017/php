<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>PDO-FETCH</title>
</head>
<body>
    <h1>PDO::FETCH_BOTH</h1>
    <hr>
    <p>
        <b>PDO::FETCH_BOTH</b> es el valor por defecto. Devuelve un array cuyas claves son los nombres de las columnas devueltas. También devuelve las claves numéricas.
    </p>
    <hr>
    <p>
        Obtenemos todas las filas de la tabla usuarios y por cada fila dos formas de acceder a los datos, mediante índice numérico o mediante el nombre de la columna obtenida.
    </p>
    <hr>
    <?php
        require_once("configuracion.php");

        $base_datos_PDO = new PDO("mysql:host=$servidor; dbname=$base_datos", $usuario, $pass);

        $resultado = $base_datos_PDO->query("SELECT * FROM usuarios");

        while ($fila = $resultado->fetch(PDO::FETCH_BOTH)) {
            print_r($fila) . '<br>';
        }
    ?>
</body>
</html>