<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>PDO-Sent-Prep</title>
</head>
<body>
    <h1>Sentencias Preparadas</h1>
    <hr>
    <p>
        Son sentencias que se ejecutan repetidamente y hacen un uso my eficiente del gestor de bases de datos. <br>
        La ejecución de <i>Sentencias Preparadas</i> consiste en: <b>la preparación</b> y <b>la ejecución</b>.
    </p>
    <hr>
    <p>
        Las consultas preparadas definen perfectamente qué partes de la consulta son lenguaje de la base de datos y qué partes son variables. <br>
        También previenen de ataques del tipo SQL Injection porque controlan todos los caracteres que se lanzan al gestor de bases de datos.
    </p>
    <hr>
    
    <?php
    require_once("configuracion.php");
    
    $base_datos_PDO = new PDO("mysql:host=$servidor; dbname=$base_datos", $usuario, $pass);

    $sql = "SELECT id, nombre, apellidos FROM usuarios WHERE id=:id";

    $sentencia = $base_datos_PDO->prepare("$sql");

    $sentencia->execute(array("id" => 1));

    while ($fila = $sentencia->fetch(PDO::FETCH_BOTH)) {
        print_r($fila) . '<br>';
    }
    ?>
</body>
</html>