<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>PDO</title>
</head>
<body>
    <h1>PDO - Actualizar o Borradas</h1>
    <hr>
    <p>
        Cuando realizamos una consulta del tipo INSERT, UPDATE o DELETE, podemos saber cuántas filas de la base de datos han sido modificadas. <br>
        Para ello se utiliza el método <b>rowCount()</b>. <br>
        El siguiente ejemplo borra las filas cuyo <i>id</i> es mayor que 3 y se obtiene el número de filas borradas.
    </p>
    <hr>
    <?php
    require_once("configuracion.php");

    $base_datos_PDO = new PDO("mysql:host=$servidor; dbname=$base_datos", $usuario, $pass);

    $sql = "DELETE FROM usuarios WHERE id > :id";
    
    $sentencia = $base_datos_PDO->prepare($sql);
    $sentencia->bindValue(":id", 3);
    $sentencia->execute();

    echo "Número de filas borradas: " . $sentencia->rowCount();
    ?>
</body>
</html>