<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Funciones</title>
</head>
<body>
    <h1>Uso de las Funciones</h1>
    <hr>
    <li>
        <ul>a imprime am o pm</ul>
        <ul>A AM ó PM</ul>
        <ul>h La hora en formato (01-12)</ul>
        <ul>H La hora en formato 24 h</ul>
        <ul>g Hora en formato 1-12 sin el 0</ul>
        <ul>G Hora en formato 24 h sin el 0</ul>
        <ul>i Minutos de 00 a 59</ul>
        <ul>s Segundos de 00 a 59</ul>
        <ul>d día del mes 01-31</ul>
        <ul>j día del mes 1-31 sin el 0</ul>
        <ul>w día de la semana 0 a 6 siendo el 0 el domingo</ul>
        <ul>m mes actual 01-12</ul>
        <ul>n mes actual 1-12 sin el 0</ul>
        <ul>Y año con 4 dígitos 2014</ul>
        <ul>Y año con 2 dígitos 14</ul>
        <ul>z Día del año 0-365</ul>
        <ul>t Número de días que tiene el mes actual</ul>
    </li>
    <hr>
    <?php
    date_default_timezone_set("UTC");
    $meses = array("Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre");

    echo("Fecha actual: " . date("d-m-Y") . "<br>");
    echo("Día del año: " . date("z") . "<br>");
    echo("Estamos en el mes: " . $meses[date("n")] . "<br>");
    ?>
</body>
</html>