<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>PHP BASICO</title>
</head>
<body>
    <h1>PHP BASICO</h1>
    <p>La función <b>isset()</b> nos indica si la variable existe y para eliminarla de la memoria necesitamos utilizar <b>unset()</b></p>
    <hr>
    <p>Las siguientes funciones nos permiten la conversión a los tipos object o array: <b>intval(), doubleval(), strval()</b></p>
    <hr>
    <!-- código PHP -->
    <?php
    $nombre = "Jose Antonio";
    $cadena = "232";

    if (isset($nombre)) {
        echo("El nombre existe");
    } else {
        echo("El nombre ya no existe!");
    }

    echo "El tipo de la variable cadena es: " . gettype($cadena) . "<br>";
    $numero = intval($cadena);
    echo "el numero es $numero";
    ?>
</body>
</html>