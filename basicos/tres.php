<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>PHP BASICOS</title>
</head>
<body>
    <H1>PHP BASICOS - CONSTANTES</H1>
    <hr>
    <p>Las constantes son tipos de datos que no varían. Para crear constantes tiene que usar la función <i><b>define()</b></i> de la siguiente forma: <i>define("LIBRO","PHP con MySQL")</i></p>
    <p>Las constantes predefinidas son accesibles por los scripts. Las principales son:</p>
    <li>
        <ol>PHP_VERSION</ol>
        <ol>PHP_OS</ol>
        <ol>PEAR_EXTENSION_DIR</ol>
        <ol>PHP_LIBDIR</ol>
        <ol>__LINE__</ol>
        <ol>__FILE__</ol>
        <ol>__FUNCTION__</ol>
        <ol>__CLASS__</ol>
        <ol>__METHOD__</ol>
    </li>

    <!-- CODIGO PHP -->
    <hr>
    <?php
    echo __LINE__ . "Version de PHP " . PHP_VERSION . "<br>";
    echo __LINE__ . "SO del servidor " . PHP_OS . "<br>";
    echo __LINE__ . "Ruta de las extensiones " . PHP_EXTENSION_DIR . "<br>";
    echo __LINE__ . "API de Servidor " . PHP_SAPI;
    ?>
    <hr>
</body>
</html>