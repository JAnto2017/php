<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Factorial</title>
</head>
<body>
<h1>Funciones</h1>
    <hr>
    <p>
        El cálculo del factorial se realiza en un bucle que va disminuyendo el valor de una variable y multiplicando todos los valores entre sí. Aprovechando este patrón puede crear una función que realice el factorial del número x ahorrando líneas de código.
    </p>
    <p>
        La función <i>factorial()</i> hace uso de la palabra reservada <i>return</i>, que es la encargada de devolver el valor que estamos solicitando cuando se invoca la función.
    </p>
    <hr>
    <br>
    <?php
    function factorial($numero) {
        $resultado = 1;
        for ($i=$numero; $i > 0; $i--) { 
            $resultado *= $i;
        }
        return $resultado;
    }
    echo("El factorial de 3 es: " . factorial(3) . "<br>");
    echo("El factorial de 4 es: " . factorial(4) . "<br>");
    echo("El factorial de 5 es: " . factorial(5) . "<br>");
    ?>

</body>
</html>