<html>
    <head>
        <title>ISSET</title>
    </head>
<body>
    <h1>Uso de las funciones ISSET() y UNSET()</h1>
    <hr>
    <p>
        La función <i><b>isset()</b></i> nos indica si la variable existe y para eliminarla de la memoria necesitamos utilizar <i><b>unset()</b></i>.
    </p>
    <hr>
    <?php
    $nombre = "José Antonio";

    if (isset($nombre)) {
        echo ("El nombre existe!");
    }
    echo ("<br>");
    unset($nombre);
    echo("Aplicamos unset()");
    echo("<br>");

    if (isset($nombre)) {
        echo ("El nombre existe!!");
    } else {
        echo ("El nombre NO existe!!");
    }
    
    ?>
</body>
</html>