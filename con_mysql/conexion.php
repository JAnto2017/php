<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Conexion MYSQL</title>
</head>
<body>
    <h1>Conexión con MYSQL</h1>
    <p>
        La conexión necesita añadir un parámetro para incluir la dirección IP del servidor de bases de datos. <br>
        Para conectar a MySQL es necesario enviar como parámetros la dirección del servidor, el usuario y la contraseña.
    </p>
    <hr>
    <?php
    $servidor = "localhost";
    $usuario = "root";
    $password = "agfingenieros";
    $base_datos = "compras";

    //conexión al servidor de base de datos
    $descriptor = mysqli_connect($servidor, $usuario, $password, $base_datos) or die ("Imposible conectar a la base de datos");

    mysqli_close($descriptor);
    ?>
</body>
</html>